import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import HomeScreen from './src/main/Home';
import LoginScreen from './src/auth/Login';
import SignUpScreen from './src/auth/SignUp';

const MainNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  Login: { screen: LoginScreen },
  SignUp: { screen: SignUpScreen }
});

const App = createAppContainer(MainNavigator);

export default App;