import React from 'react';
import { StyleSheet, Button, View } from 'react-native';
import { Container } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

import Loading from './Loading';
import MainHeading from './MainHeading';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isReady: false,
    };
  }

  async componentDidMount() {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    setTimeout(() => {
      this.setState({ isReady: true });
    }, 1000)
  }

  render() {

    const { navigate } = this.props.navigation;

    if (!this.state.isReady) {
      return <Loading />;
    }

    return (
      <Container>
        <MainHeading />

        <View style={styles.container}>
          <Button
            color="#f194ff"
            onPress={() => navigate("Login")}
            title="이메일 로그인">
          </Button>
        </View>

        <View style={styles.container}>
          <Button
            onPress={() => navigate("SignUp")}
            title="새로운 계정생성">
          </Button>
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: '#666666',
    width: '80%',
    marginLeft: '10%',
    marginRight: '10%',
    marginTop: 20,
    backgroundColor: '#ffffff'
  },
  button: {
  }
})