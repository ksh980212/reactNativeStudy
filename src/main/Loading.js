import React, { Component } from 'react';
import { Image } from 'react-native';
import { Container, Content, Card, CardItem } from 'native-base';

export default class Loading extends Component {
  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem cardBody>
              <Image source={require('../../public/image/loading.jpg')} />
            </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}