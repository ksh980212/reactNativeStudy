import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const MainHeading = () => {
    return (
        <View style={styles.container}>
            <Text style={styles.mainLogo}>Toy Project</Text>
        </View>
    );
}

export default MainHeading;

const styles = StyleSheet.create({
    container: {
        height: 200
    },
    mainLogo: {
        color: '#666666',
        textAlign: 'center',
        fontSize: 40
    }
})