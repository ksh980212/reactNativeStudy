import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { Button, Container, Content, Form, Item, Input, Label, Icon } from 'native-base';

const SignUp = () => {

  const [id, setId] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirm, setPasswordConfirm] = useState('');

  const idOnChange = (e) => {
    setId(e.nativeEvent.text);
  }

  const passwordOnChange = (e) => {
    setPassword(e.nativeEvent.text);
  }

  const passwordConfirmOnChange = (e) => {
    setPasswordConfirm(e.nativeEvent.text);
  }


  const buttonOnClick = () => {
    console.log(id, password, passwordConfirm);
  }

  return (
    <Container style={styles.container}>
      <Content>
        <Form>
          <Item stackedLabel>
            <Label>이메일</Label>
            <Input
              value={id}
              onChange={idOnChange}
              placeholder="ksh980212@gmail.com" />
          </Item>
          <Item stackedLabel last>
            <Label>비밀번호</Label>
            <Input
              onChange={passwordOnChange}
              value={password}
              placeholder="********" />
          </Item>
          <Item stackedLabel last>
            <Label>비밀번호 재입력</Label>
            <Input
              onChange={passwordConfirmOnChange}
              value={passwordConfirm}
              placeholder="********" />
          </Item>
          <Button iconRight light
            style={styles.icon}
            onPress={buttonOnClick} >
            <Icon name='arrow-forward' />
          </Button>
        </Form>
      </Content>

    </Container>
  );
}

const styles = StyleSheet.create({
  container: {
    marginTop: 100,
    marginLeft: 50,
    marginRight: 50
  },
  icon: {
    alignSelf: 'center'
  }
})

export default SignUp;